import unittest
from unittest.mock import patch

from app.vessels.vessel import Vessels


# class TestVessels(unittest.TestCase):
#
#     @patch('app.vessels.vessel.input')
#     @patch('app.vessels.vessel.print')
#     def test_vessels_options(self, mock_print, mock_input_vessel, mock_input_position):
#         mock_print.return_value = ['Aircraft Carrier', 'Battleship', 'Cruiser',
#                                    'Destroyer', 'Destroyer', 'Submarines', 'Submarines']
#         mock_input_vessel.side_effect = ['1', '2', '3', '4', '4', '5', '5']
#         mock_input_position.side_effect = ['A2', 'B1', 'C3', 'D5', 'F6', 'G1', 'J5']
#
#         vessels = Vessels()
#
#         vessels.vessels_options()
#
#         list_options = [['Aircraft Carrier', 5, 'A2', 'A3', 'A4', 'A5', 'A6'], ['Battleship', 4, 'B1', 'B2', 'B3', 'B4'],
#                         ['Cruiser', 3, 'C3', 'C4', 'C5'], ['Destroyer', 2, 'D5', 'D6'], ['Destroyer', 2, 'F6', 'F7'],
#                         ['Submarines', 1, 'G1'], ['Submarines', 1, 'J5']]
#         available_vessels = []
#
#         self.assertEqual()
