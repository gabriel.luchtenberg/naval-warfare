from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session
import os

database_name = os.path.join(os.getcwd(), 'db.sqlite3')

engine = create_engine(f'sqlite:///{database_name}', echo=True)
session = scoped_session(sessionmaker(bind=engine))

Base = declarative_base()
Base.query = session.query_property()
