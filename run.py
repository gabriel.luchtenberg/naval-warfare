from exceptions.players.run import LoadingFiles
from app.vessels.vessel import Vessels


settings = Vessels()
settings.call_everything()
matrix = settings.generating_matrix()

matrix1 = settings.matrix
vessel1 = settings.vessels
player_1 = settings.player

settings = Vessels()
settings.call_everything()

matrix2 = settings.matrix
vessel2 = settings.vessels
player_2 = settings.player

verification = LoadingFiles(matrix, vessel1, vessel2, matrix1, matrix2, player_1, player_2)
verification.execute_game()

if verification.execute_game == player_1:
    print(f'{player_1} Win the game!!')

elif verification.execute_game == player_2:
    print(f'{player_2} Win the game!!')
