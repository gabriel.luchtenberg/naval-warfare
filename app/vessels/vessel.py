from app.boards.board import Board
from string import ascii_uppercase


class Vessels(Board):

    def vessels_options(self):
        available_vessels = ['Aircraft Carrier', 'Battleship', 'Cruiser',
                             'Destroyer', 'Destroyer', 'Submarines', 'Submarines']
        option_vessels = ['1', 'Aircraft Carrier', 5], ['2', 'Battleship', 4], \
                         ['3', 'Cruiser', 3], ['4', 'Destroyer', 2], ['5', 'Submarines', 1]
        while len(available_vessels) > 0:
            print(available_vessels)
            vessel = input('Enter the desired vessel number: ')
            position = input('Vessel Position: ').upper()
            for i in range(len(option_vessels)):
                if vessel in option_vessels[i]:
                    list_options = [option_vessels[i][1], option_vessels[i][2]]
                    for abc in ascii_uppercase:
                        if abc in position:
                            for p in range(int(position.strip(abc)), int(position.strip(abc)) + option_vessels[i][2]):
                                list_options.append(abc + str(p))
                            self.vessels.append(list_options)
                            option = option_vessels[i][1]
                            available_vessels.remove(option)

    def position_vessels(self):
        for lin in range(10):
            for c in range(10):
                for i in self.vessels:
                    if i[2] in self.matrix[lin][c][0]:
                        for p in range(i[1]):
                            if i[1] < len(self.matrix[lin][c]):
                                self.matrix[lin][c + p] = [i[0]]

    def call_everything(self):
        self.generating_matrix()
        self.vessels_options()
        self.position_vessels()
        self.displays_matrix()
