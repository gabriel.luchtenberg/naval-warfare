from string import ascii_uppercase


class Board:
    def __init__(self):
        self.matrix = []
        self.vessels = []
        self.player = input('Type your name: ')

    def generating_matrix(self):
        column = []
        row = []
        for x in range(10):
            for y in range(10):
                row.append([ascii_uppercase[x] + str(y + 1)])
            column.append(row.copy())
            row.clear()
        self.matrix = column.copy()
        return self.matrix

    def displays_matrix(self):
        for each in self.matrix:
            print(each)
