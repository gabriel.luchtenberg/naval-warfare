class Players:

    def __init__(self, matrix, vessel1, vessel2, matrix1, matrix2, player1, player2):

        self.vessel1 = vessel1
        self.vessel2 = vessel2
        self.matrix1 = matrix1
        self.matrix2 = matrix2
        self.player1 = player1
        self.player2 = player2
        self.matrix = matrix

    def player_1(self):
        print(self.player1)
        move = input(f'Move {self.player1}\n>> ').upper()
        for i in self.vessel2:
            if move in i:
                for lin in range(10):
                    for c in range(9):
                        if move in self.matrix[lin][c][0]:
                            self.matrix2[lin][c] = ['💥']
                            return True

    def player_2(self):
        print(self.player2)
        move = input(f'Move {self.player2}\n>> ').upper()
        for i in self.vessel1:
            if move in i:
                for lin in range(10):
                    for c in range(9):
                        if move in self.matrix[lin][c][0]:
                            self.matrix1[lin][c] = ['💥']
                            return True
