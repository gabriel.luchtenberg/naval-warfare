from sqlalchemy import Column, ForeignKey, Integer, String, Table, Text
from database import Base
from sqlalchemy.orm import relationship


class Ship(Base):
    __tablename__ = 'Vessels'

    name = Column(String, primary_key=True)
    size = Column(Integer)

    def __repr__(self):
        return f"Ship(name={self.name}, size={self.size})"


class AircraftCarrier(Ship):
    def __init__(self):
        super().__init__(name='Aircraft Carrier', size=5)


class Battleship(Ship):
    def __init__(self):
        super().__init__(name='Battleship', size=4)


class Cruiser(Ship):
    def __init__(self):
        super().__init__(name='Cruiser', size=3)


class Destroyer(Ship):
    def __init__(self):
        super().__init__(name='Destroyer', size=2)


class Submarine(Ship):
    def __init__(self):
        super().__init__(name='Submarine', size=1)
