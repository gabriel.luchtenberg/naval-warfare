from sqlalchemy import engine

from app.db.vessels.models import Ship, AircraftCarrier, Battleship, Cruiser, Destroyer, Submarine
from database import session, Base, engine

if __name__ == '__main__':
    Base.metadata.create_all(engine)

    AircraftCarrier = AircraftCarrier()
    session.add(AircraftCarrier)

    Battleship = Battleship()
    session.add(Battleship)

    Cruiser = Cruiser()
    session.add(Cruiser)

    Destroyer = Destroyer()
    session.add(Destroyer)

    Submarine = Submarine()
    session.add(Submarine)

    session.commit()
