from sqlalchemy import Column, ForeignKey, Integer, String, Table, Text
from database import Base
from sqlalchemy.orm import relationship


class Players(Base):
    __tablename__ = 'Players'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)

    def __repr__(self):
        return f"<Ship(id={self.id}, name={self.name})>"
